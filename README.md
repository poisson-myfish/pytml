# pyTML
A **Python** to **HTML** compiler, so that you can write **HTML** more easily.

## Installation
There's no installation, you just copy the `pyweb.py` file from the root of the **main** branch and put it in your project. Then, import everything from it in a new **Python** file and start writing.

## Examples
You can find an example in the /examples directory.