from pytml import *

docinit()
title('Welcome to pyTML')

headin()
style('main.css')
headline(size=6, text='Welcome to pyTML!')
headout()

bodyin()
paragraph(text="Here's an anchor:")
anchor(text='Go to google', link='https://www.google.com')
bodyout()

customtagin('footer')
paragraph(text="that's my footer")
customtagout('footer')
