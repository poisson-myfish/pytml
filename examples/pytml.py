file = open('index.html', 'w+')

def docinit(): 
    file.write('<!DOCTYPE html> \n\n')

def headline(text, size):
    if size == 6:
        file.write(f'<h1>{text}</h1> \n')
    elif size == 5:
        file.write(f'<h2>{text}</h2> \n')
    elif size == 4:
        file.write(f'<h3>{text}</h3> \n')
    elif size == 3:
        file.write(f'<h4>{text}</h4> \n')
    elif size == 2:
        file.write(f'<h5>{text}</h5> \n')
    elif size == 1:
        file.write(f'<h6>{text}</h6> \n')
    else:
        print('ERROR: headline size not defined')
        exit()
    

def anchor(text, link):
    file.write(f'<a href="{link}">{text}</a> \n')

def paragraph(text):
    file.write(f'<p>{text}</p> \n')

def style(path):
    file.write(f'<link type="text/css" rel="stylesheet" href="{path}"> \n')

def headin():
    file.write('<head> \n')

def headout():
    file.write('</head> \n\n')

def bodyin():
    file.write('<body> \n')

def bodyout():
    file.write('</body> \n\n')

def customtagin(tag):
    file.write(f'<{tag}> \n')

def customtagout(tag):
    file.write(f'</{tag}> \n\n')

def title(text):
    file.write(f'<title>{text}</title>')
